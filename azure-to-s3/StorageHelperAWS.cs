﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Configuration;
using System.IO;
using System.Drawing;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon;
using System.Threading.Tasks;
using Amazon.S3.Transfer;
using Amazon.Runtime;

namespace azure_to_s3
{
  public static class StorageHelperAWS
  {
    public static void upload(string profileName, string bucketName, string path, byte[] file, string root = "QA/")
    {
      // Prefix fix
      if (root[root.Length - 1] != '/' && root.Length > 0)
      {
        root += "/";
      }

      // var profile = new StoredProfileAWSCredentials()
      var client = new AmazonS3Client();

      try
      {
        PutObjectRequest putRequest = new PutObjectRequest
        {
          BucketName = bucketName,
          Key = root + path,
          InputStream = new MemoryStream(file)
        };

        PutObjectResponse response = client.PutObject(putRequest);
      }
      catch (AmazonS3Exception amazonS3Exception)
      {
        if (amazonS3Exception.ErrorCode != null &&
                    (amazonS3Exception.ErrorCode.Equals("InvalidAccessKeyId")
                    ||
                    amazonS3Exception.ErrorCode.Equals("InvalidSecurity")))
        {
          throw new Exception("Check the provided AWS Credentials.");
        }
        else
        {
          throw new Exception("Error occurred: " + amazonS3Exception.Message);
        }
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
    }

  }
}
