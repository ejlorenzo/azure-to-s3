﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Text.RegularExpressions;

namespace azure_to_s3
{
  class Utils
  {
    public static byte[] getFile(string path)
    {
      System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
      return new WebClient().DownloadData(path);
    }

    public static String extractFilename(string input)
    {
      Regex r1 = new Regex(@"\.net\/(.+)([0-9a-f\-]{36}\.[a-z]{3,})");

      Match match = r1.Match(input);

      if (match.Success)
      {
        return String.Format("{0}{1}", match.Groups[1].Value, match.Groups[2].Value);
      }

      throw new Exception("Invalid file");
    }
  }
}
