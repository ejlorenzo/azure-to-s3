﻿namespace azure_to_s3
{
  partial class Form1
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.password = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.username = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.database = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.host = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.prefix = new System.Windows.Forms.TextBox();
      this.label7 = new System.Windows.Forms.Label();
      this.profile = new System.Windows.Forms.TextBox();
      this.label5 = new System.Windows.Forms.Label();
      this.bucket = new System.Windows.Forms.TextBox();
      this.label6 = new System.Windows.Forms.Label();
      this.groupBox3 = new System.Windows.Forms.GroupBox();
      this.query = new System.Windows.Forms.TextBox();
      this.btnRun = new System.Windows.Forms.Button();
      this.refreshCompletedBarTimer = new System.Windows.Forms.Timer(this.components);
      this.progressBar = new System.Windows.Forms.ProgressBar();
      this.logger = new System.Windows.Forms.TextBox();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.password);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Controls.Add(this.username);
      this.groupBox1.Controls.Add(this.label3);
      this.groupBox1.Controls.Add(this.database);
      this.groupBox1.Controls.Add(this.label2);
      this.groupBox1.Controls.Add(this.host);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Location = new System.Drawing.Point(12, 12);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(288, 141);
      this.groupBox1.TabIndex = 0;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "SQL Configuration";
      // 
      // password
      // 
      this.password.Location = new System.Drawing.Point(80, 106);
      this.password.Name = "password";
      this.password.Size = new System.Drawing.Size(194, 20);
      this.password.TabIndex = 7;
      this.password.UseSystemPasswordChar = true;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(6, 109);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(53, 13);
      this.label4.TabIndex = 6;
      this.label4.Text = "Password";
      // 
      // username
      // 
      this.username.Location = new System.Drawing.Point(80, 80);
      this.username.Name = "username";
      this.username.Size = new System.Drawing.Size(194, 20);
      this.username.TabIndex = 5;
      this.username.Text = "RDS_3DMS_admin";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(6, 83);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(55, 13);
      this.label3.TabIndex = 4;
      this.label3.Text = "Username";
      // 
      // database
      // 
      this.database.Location = new System.Drawing.Point(80, 54);
      this.database.Name = "database";
      this.database.Size = new System.Drawing.Size(194, 20);
      this.database.TabIndex = 3;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(6, 57);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(53, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "Database";
      // 
      // host
      // 
      this.host.Location = new System.Drawing.Point(80, 28);
      this.host.Name = "host";
      this.host.Size = new System.Drawing.Size(194, 20);
      this.host.TabIndex = 1;
      this.host.Text = "rds-3dms-qa-db01.cm5tgtqy0uuz.us-east-1.rds.amazonaws.com";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(6, 31);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(29, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Host";
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.prefix);
      this.groupBox2.Controls.Add(this.label7);
      this.groupBox2.Controls.Add(this.profile);
      this.groupBox2.Controls.Add(this.label5);
      this.groupBox2.Controls.Add(this.bucket);
      this.groupBox2.Controls.Add(this.label6);
      this.groupBox2.Location = new System.Drawing.Point(306, 12);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(288, 141);
      this.groupBox2.TabIndex = 8;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "AWS Configuration";
      // 
      // prefix
      // 
      this.prefix.Location = new System.Drawing.Point(79, 80);
      this.prefix.Name = "prefix";
      this.prefix.Size = new System.Drawing.Size(194, 20);
      this.prefix.TabIndex = 9;
      this.prefix.Text = "QA";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(5, 83);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(33, 13);
      this.label7.TabIndex = 8;
      this.label7.Text = "Prefix";
      // 
      // profile
      // 
      this.profile.Location = new System.Drawing.Point(79, 54);
      this.profile.Name = "profile";
      this.profile.Size = new System.Drawing.Size(194, 20);
      this.profile.TabIndex = 7;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(5, 57);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(36, 13);
      this.label5.TabIndex = 6;
      this.label5.Text = "Profile";
      // 
      // bucket
      // 
      this.bucket.Location = new System.Drawing.Point(79, 28);
      this.bucket.Name = "bucket";
      this.bucket.Size = new System.Drawing.Size(194, 20);
      this.bucket.TabIndex = 5;
      this.bucket.Text = "3dms-storage";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(5, 31);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(41, 13);
      this.label6.TabIndex = 4;
      this.label6.Text = "Bucker";
      // 
      // groupBox3
      // 
      this.groupBox3.Controls.Add(this.query);
      this.groupBox3.Location = new System.Drawing.Point(12, 159);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new System.Drawing.Size(582, 141);
      this.groupBox3.TabIndex = 8;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "SQL Query";
      // 
      // query
      // 
      this.query.Location = new System.Drawing.Point(9, 19);
      this.query.Multiline = true;
      this.query.Name = "query";
      this.query.Size = new System.Drawing.Size(567, 116);
      this.query.TabIndex = 1;
      this.query.Text = "select top 3 * from dbo.attachments";
      // 
      // btnRun
      // 
      this.btnRun.Location = new System.Drawing.Point(12, 421);
      this.btnRun.Name = "btnRun";
      this.btnRun.Size = new System.Drawing.Size(580, 32);
      this.btnRun.TabIndex = 9;
      this.btnRun.Text = "Run";
      this.btnRun.UseVisualStyleBackColor = true;
      this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
      // 
      // refreshCompletedBarTimer
      // 
      this.refreshCompletedBarTimer.Enabled = true;
      this.refreshCompletedBarTimer.Tick += new System.EventHandler(this.refreshCompletedBar_Tick);
      // 
      // progressBar
      // 
      this.progressBar.Location = new System.Drawing.Point(12, 459);
      this.progressBar.Name = "progressBar";
      this.progressBar.Size = new System.Drawing.Size(580, 32);
      this.progressBar.TabIndex = 10;
      // 
      // logger
      // 
      this.logger.Location = new System.Drawing.Point(12, 306);
      this.logger.Multiline = true;
      this.logger.Name = "logger";
      this.logger.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.logger.Size = new System.Drawing.Size(580, 109);
      this.logger.TabIndex = 11;
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(604, 501);
      this.Controls.Add(this.logger);
      this.Controls.Add(this.progressBar);
      this.Controls.Add(this.btnRun);
      this.Controls.Add(this.groupBox3);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.Name = "Form1";
      this.Text = "Azure to S3";
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.TextBox password;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox username;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox database;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox host;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.TextBox profile;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.TextBox bucket;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.TextBox query;
    private System.Windows.Forms.Button btnRun;
    private System.Windows.Forms.Timer refreshCompletedBarTimer;
    private System.Windows.Forms.ProgressBar progressBar;
    private System.Windows.Forms.TextBox prefix;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.TextBox logger;
  }
}

