﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace azure_to_s3
{
  class DatabaseHelper
  {
    private String host;
    private String database;
    private String username;
    private String password;

    public DatabaseHelper(String host, String database, String username, String password)
    {
      this.host = host;
      this.database = database;
      this.username = username;
      this.password = password;
    }

    public List<String> query(String query)
    {
      List<String> urls = new List<String>();
      SqlConnection connection = new SqlConnection(String.Format("Data Source={0}, 1433;Network Library=DBMSSOCN;Initial Catalog={1};Password={3};User ID={2}", 
        this.host, this.database, this.username, this.password));

      SqlCommand cmd = new SqlCommand(query, connection);
      connection.Open();

      using (SqlDataReader reader = cmd.ExecuteReader())
      {
        while (reader.Read())
        {
          urls.Add(reader["path"].ToString());
        }
      }

      return urls;
    }
  }
}
