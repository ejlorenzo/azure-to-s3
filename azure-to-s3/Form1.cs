﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace azure_to_s3
{
  public partial class Form1 : Form
  {
    private int current = 0, total = 1;
    private Thread t;
    private String _logger = "";

    // Lazyness
    private String _host, _database, _username, _password, _query, _profile, _bucket, _prefix;

    public Form1()
    {
      InitializeComponent();
    }

    private void refreshCompletedBar_Tick(object sender, EventArgs e)
    {
      progressBar.Maximum = this.total;
      progressBar.Value = this.current;

      if (t == null)
      {
        this.btnRun.Enabled = true;
        this.query.Enabled = true;
      }
      else
      {
        this.btnRun.Enabled = false;
        this.query.Enabled = false;
      }

      if (this.logger.Text != this._logger)
      {
        this.logger.Text = _logger;
      }
    }

    private void btnRun_Click(object sender, EventArgs e)
    {
      // Set current values
      this._host = this.host.Text;
      this._database = this.database.Text;
      this._username = this.username.Text;
      this._password = this.password.Text;
      this._query = this.query.Text;
      this._profile = this.profile.Text;
      this._bucket = this.bucket.Text;
      this._prefix = this.prefix.Text;

      // Run
      this.t = new Thread(run);
      t.Start();
    }

    private void run()
    {
      DatabaseHelper dbh;
      List<String> urls = new List<string>();

      try
      {
        dbh = new DatabaseHelper(this._host, this._database, this._username, this._password);
        urls = dbh.query(this._query);
      } catch (Exception e)
      {
        MessageBox.Show(e.Message, "Error initializing DatabaseHandler", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }

      // Reset progress bar
      this.current = 0;
      this.total = urls.Count;

      foreach (String s in urls)
      {
        try
        {
          StorageHelperAWS.upload(this._profile, this._bucket, Utils.extractFilename(s), Utils.getFile(s), this._prefix);
        }
        catch (Exception e)
        {
          this._logger += "[" + DateTime.Now.ToFileTime() + "] Error uploading file to AWS" + Environment.NewLine
            + "Reason: " + e.Message + Environment.NewLine
            + "URL: " + s + Environment.NewLine;

          // MessageBox.Show(e.Message, "Error Uploading file to AWS", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        current++;
        Thread.Sleep(1);
      }

      MessageBox.Show("Process completed", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
      this.current = 0;
      this.t = null;
    }
  }
}
